all: netfileserver 

netfileserver: netfileserver.c
	gcc -pthread -Wall -o netfileserver netfileserver.c

clean:
	rm -f netfileserver
