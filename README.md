Tsun Ming Yu
Pengrui Liu

netfileserver.c
The purpose of this program is to create a server for client users to connect to and
perform a remote file open, read, write and close.

The way this server was implemented is to open socket and continuously accept
incoming connections from clients. Every time a connection has been connected, the server will
create a new kernel thread and pass the socket file descriptor to the thread created. As soon as
the thread enter its function, the server will read an instruction message coming from the
client. The message will be in the following form.

(a letter indicating instruction mode)|………..

This letter will be either ‘o’ for open, ‘r’ for read, ‘w’ for write and ‘c’ for close. From
there the server will call the respective functions and perform the correct actions.

In the fileopen() function, the server will read a file pathname received from the client
and open the file. If an error occurs, the function will send back an error message starting with
the letter e, followed by an errno number.

In fileread(), the server will take in the file descriptor received from the client as well as
the number of bytes that the client wants to read. If the file encountered an error, it will send
an error message back to the client. If it succeeded, it will send back the message that was read.


In filewrite(), the server will also take in the file descriptor. It will also take in the
message that it wants to write in, as well as the number of bytes that it wants to read. If it
encounters and error, it will do the same thing as the other functions. If it succeeded, the server
will return the number of bytes that was written.

In fileclose(), the server will close the file, and do the correct error checking and send
corresponding messages.

After each functions performed, the server will close that socket that was connected.



libnetfiles.c

This program is a library that can be used by the client to run the remote file operations.
This program contains 5 functions: netserverinit(), netopen(), netread(), netwrite(), netclose().
The netserverinit() is a function that will take in a hostname. This hostname must be same as
the location of the server created. If the host is not found, the program will report and error
and return. After that, all the other programs will build a new client socket and connect to the
server. Once the function is connected to the server successfully, the program will build a
message for the server. The first letter of the message will always be the instructions for the
server: open, read, write, or close. After that, it will be all the required arguments that is
required for the operation. These arguments will be converted to string and send to the server.
If the server encounters the error, the server will send an error message code back and the
library function will read the errno number, print the error and exit the program.

In order to use this library, you need to include libnetfiles.c when you compile your client.