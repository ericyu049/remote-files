#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include "libnetfiles.h"

struct sockaddr_in SAInfo;	
struct hostent* hp;
int portno = 8888;
extern int h_errno;
char* itoa(int value, char* result, int base) {
	// check that the base if valid
	if (base < 2 || base > 36) {
		*result = '\0'; 
		return result;
	}
	char* ptr = result, *ptr1 = result, tmp_char;
	int tmp_value;
	do {
		tmp_value = value;
		value /= base;
		*ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
	}while(value);

    // Apply negative sign
    if (tmp_value < 0){
    	*ptr++ = '-';
    }
    *ptr-- = '\0';
    while(ptr1 < ptr) {
    	tmp_char = *ptr;
    	*ptr--= *ptr1;
    	*ptr1++ = tmp_char;
    }
    return result;
}
int netserverinit (const char* hostname){
	if (gethostbyname(hostname) == NULL){
		printf("gethostbyname() error for host: %s: %s\n", hostname, hstrerror(h_errno));
		return -1;
	}
    else{
		hp = gethostbyname(hostname);
		bzero((char*)&SAInfo, sizeof(SAInfo));
		SAInfo.sin_family = AF_INET;
		SAInfo.sin_port = htons(portno);
		bcopy((char*)hp->h_addr, (char*)&SAInfo.sin_addr.s_addr, hp->h_length);
		return 0;
	}
}
int netopen (const char* pathname, int flags){
	//Build a new client socket (using global hostent)
	int sockfd = -1;
	char buffer [256];
	int n = -1;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        error("ERROR creating socket");
	}
	//Connect to file server
	if (connect(sockfd,(struct sockaddr*)&SAInfo,sizeof(SAInfo)) < 0){
        error("ERROR connecting");
	}	
	//Send mesg
    bzero(buffer,256);
	buffer[0] = 'o';
	buffer[1] = '|';
	int i = 2;
	int j = 0;
	while (j<strlen(pathname)){
		buffer[i] = pathname[j];
		i++;
		j++;
	}
	buffer[i] = '|';
	i++;
	if (flags == O_RDONLY){
		buffer[i] = '0';
	}
	else if (flags == O_WRONLY){
		buffer[i] = '1';
	}
	else if (flags == O_RDWR){
		buffer[i] = '2';
	}
	n = write(sockfd,buffer,strlen(buffer));
    if (n < 0){
         error("ERROR writing to socket");
    }
	//Get response
	bzero(buffer,256);
    n = read(sockfd,buffer,255);
    if (n < 0){
         error("ERROR reading from socket");
	}
	//Set appropriate vals
	char errormsg [256];
	if (buffer[0] == 'e'){	
		int i = 1;
		int j = 0;
		while (buffer[i] != '\0'){
			errormsg[j] = buffer[i];
			i++;
			j++;
		}
		errno = atoi(errormsg);
		printf("Error: %s\n", strerror(errno));
		close(sockfd);
		return -1;
	}
	else {
		int filedescriptor = atoi(buffer);
		close(sockfd);
		return filedescriptor;
	}
}
ssize_t netread(int fildes, void *buf, size_t nbyte){
	//Build a new client socket (using global hostent)
	if (fcntl(fildes, F_GETFD) != -1){
		puts("hello\n");
		errno = 9;
		printf("Error: %s\n", strerror(errno));
		return -1;
	}
	int sockfd = -1;
	char buffer [256];
	int n = -1;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        error("ERROR creating socket");
	}
	//Connect to file server
	if (connect(sockfd,(struct sockaddr*)&SAInfo,sizeof(SAInfo)) < 0){
        error("ERROR connecting");
	}	
	//Send mesg
    bzero(buffer,256);
	buffer[0] = 'r';
	buffer[1] = '|';
	char* filedescriptor = (char*) malloc (256);
	itoa (fildes, filedescriptor, 10);
	int i = 0;
	int j = 2;
	while (i < strlen(filedescriptor)){
		buffer[j] = filedescriptor[i];
		i++;
		j++;
	}
	buffer[j] = '|';
	j++;
	char* numbytes = (char*) malloc (256);
	itoa (nbyte, numbytes, 10);
	int k = 0;
	while (k < strlen(numbytes)){
		buffer[j] = numbytes[k];
		k++;
		j++;
	}
	n = write(sockfd, buffer, strlen(buffer));
	if (n < 0){
		error("ERROR writing to socket");
	}
	//Get response
	n = read (sockfd, buffer, 256);
	if (n < 0){
		error("ERROR reading from socket");
	}
	char errormsg [256];
	if (buffer[0] == 'e'){
		int i = 1;
		int j = 0;
		while (buffer[i] != '\0'){
			errormsg[j] = buffer[i];
			i++;
			j++;
		}
		errno = atoi(errormsg);
		printf("Error: %s\n", strerror(errno));
		close(sockfd);
		return -1;
	}
	else {
		char numberbytes [256];
		int e = 0;
		while (buffer[e] != '|'){
			numberbytes[e] = buffer[e];
			e++;
		}
		e++;
		numberbytes[e] = '\0';
		int nb = atoi (numberbytes);
		int x = 0;
		char* bxf = (char*)buf;
		while (x < nb){
			bxf[x] = buffer[e];
			e++;
			x++;
		}
		strncpy(buf, bxf, nb);
		close(sockfd);
		return nb;
	}
}
ssize_t netwrite(int fildes, const void *buf, size_t nbyte){
	//puts("hello");
	if (fcntl(fildes, F_GETFD) != -1){
		puts("hello\n");
		errno = 9;
		printf("Error: %s\n", strerror(errno));
		return -1;
	}
	//Build a new client socket (using global hostent)
	int sockfd = -1;
	char buffer [256+nbyte];
	int n = -1;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        error("ERROR creating socket");
	}
	//Connect to file server
	if (connect(sockfd,(struct sockaddr*)&SAInfo,sizeof(SAInfo)) < 0){
        error("ERROR connecting");
	}
	//Send msg
	bzero(buffer, nbyte);
	buffer[0] = 'w';
	buffer[1] = '|';
	char* filedescriptor = (char*) malloc (256);
	itoa (fildes, filedescriptor, 10);
	int i = 0;
	int j = 2;
	while (i < strlen(filedescriptor)){
		buffer[j] = filedescriptor[i];
		i++;
		j++;
	}
	buffer[j] = '|';
	j++;
	char* numbytes = (char*) malloc (256);
	itoa (nbyte, numbytes, 10);
	int k = 0;
	while (k < strlen(numbytes)){
		buffer[j] = numbytes[k];
		k++;
		j++;
	}
	buffer[j] = '|';
	char* bufx = (char*) malloc (nbyte+1);
	strncpy(bufx, (char*)buf, nbyte);
	bufx[nbyte] = '\0';
	j++;
	k = 0;
	while (k < strlen(bufx)){
		buffer[j] = bufx[k];
		k++;
		j++;
	}
	n = write(sockfd, buffer, strlen(buffer));
	if (n < 0){
		error("ERROR writing to socket");
	}
	//Get response
	bzero(buffer, 256);
	n = read (sockfd, buffer, 255);
	if (n < 0){
		error("ERROR reading from socket");
	}
	char errormsg [256];
	if (buffer[0] == 'e'){
		int i = 1;
		int j = 0;
		while (buffer[i] != '\0'){
			errormsg[j] = buffer[i];
			i++;
			j++;
		}
		errno = atoi(errormsg);
		printf("Error: %s\n", strerror(errno));
		close(sockfd);
		return -1;
	}
	else {
		printf("File written!\n");
		int ret = atoi (buffer);
		close(sockfd);
		return ret;
	}
}
int netclose (int fildes){
	if (fcntl(fildes, F_GETFD) != -1){
		puts("hello\n");
		errno = 9;
		printf("Error: %s\n", strerror(errno));
		return -1;
	}
	//Build a new client socket (using global hostent)
	int sockfd = -1;
	char buffer [256];
	int n = -1;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        error("ERROR creating socket");
	}
	//Connect to file server
	if (connect(sockfd,(struct sockaddr*)&SAInfo,sizeof(SAInfo)) < 0){
        error("ERROR connecting");
	}	
	//Send mesg
    bzero(buffer,256);
	buffer[0] = 'c';
	buffer[1] = '|';
	char* filedescriptor = (char*) malloc (256);
	itoa (fildes, filedescriptor, 10);
	int i = 0;
	int j = 2;
	while (i < strlen(filedescriptor)){
		buffer[j] = filedescriptor[i];
		i++;
		j++;
	}
	buffer[j] = '\0';
	n = write(sockfd, buffer, strlen(buffer));
	if (n < 0){
		error("ERROR writing to socket");
	}
	//Get responses
	bzero(buffer, 256);
	n = read (sockfd, buffer, 255);
	if (n < 0){
		error("ERROR reading from socket");
	}
	//Error checks and results
	char errormsg [256];
	if (buffer[0] == 'e'){
		int i = 1;
		int j = 0;
		while (buffer[i] != '\0'){
			errormsg[j] = buffer[i];
			i++;
			j++;
		}
		errno = atoi(errormsg);
		printf("Error: %s\n", strerror(errno));
		close(sockfd);
		return -1;
	}
	else if (buffer[0] == '0') {
		close(sockfd);
		return 0;
	}
}
