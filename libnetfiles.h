#ifndef NETLIBFILES_H
#define NETLIBFILES_H

int netserverinit (const char* hostname);
int netopen (const char* pathname, int flags);
ssize_t netread(int fildes, void *buf, size_t nbyte);
ssize_t netwrite(int fildes, const void *buf, size_t nbyte);
int netclose (int fildes);



#endif