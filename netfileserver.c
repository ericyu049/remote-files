#include <stdio.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>

pthread_mutex_t lock;
extern int errno;
int connectionFD [1024];
int fd;

char* itoa(int value, char* result, int base) {
	// check that the base if valid
	if (base < 2 || base > 36) {
		*result = '\0'; 
		return result;
	}
	char* ptr = result, *ptr1 = result, tmp_char;
	int tmp_value;
	do {
		tmp_value = value;
		value /= base;
		*ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
	}while(value);

    // Apply negative sign
    if (tmp_value < 0){
    	*ptr++ = '-';
    }
    *ptr-- = '\0';
    while(ptr1 < ptr) {
    	tmp_char = *ptr;
    	*ptr--= *ptr1;
    	*ptr1++ = tmp_char;
    }
    return result;
}
void error(char *msg){
    perror(msg);
    exit(1);
}
int fileopen (char* buffer){
	int n = -1;
	char* pathname = (char*)malloc(256);
	int i = 2;
	int j = 0;
	while (buffer[i] != '|'){
		pathname[j] = buffer[i];
		i++;
		j++;
	}
	int fdx;
	int flag = atoi(&buffer[i+1]);
	if (flag == 0){
		fdx = open(pathname, O_RDONLY);
	}
	else if (flag == 1){
		fdx = open(pathname, O_WRONLY);
	}
	else if (flag == 2){
		fdx = open(pathname, O_RDWR);
	}
	if (fdx == -1){
		char* retVal = (char*)malloc(256);
		itoa(errno, retVal, 10);
		char e [2];
		e[0] = 'e';
		e[1] = '\0';
		strcat(e, retVal);
		n = write(fd, e, strlen(e));
		if (n < 0){
			error ("ERROR writing to socket.");
		}
		return -1;
	}
	else{
		itoa (fdx, (char*)buffer, 10);
		n = write (fd, buffer, strlen(buffer));
		if (n < 0){
			error ("ERROR writing to socket.");
		}
	}	
	return fd;
}
int fileread(char* buffer){
	int n = -1;
	buffer += 2;
	int i = 0;
	char* filedescriptor = (char*) malloc (256);
	while (buffer[i] != '|'){
		filedescriptor[i] = buffer[i];
		i++;
	}
	int fdes = atoi (filedescriptor);
	i++;
	char* numbytes = (char*) malloc (256);
	int j = 0;
	while (buffer[i] != '\0'){
		numbytes[j] = buffer[i];
		i++;
		j++;
	}
	int numberbytes = atoi (numbytes);
	char storage [numberbytes+1];
	bzero(storage, numberbytes);
	int errcheck = read (fdes, storage, numberbytes);
	if (errcheck < 0){
		char* retVal = (char*)malloc(256);
		itoa(errno, retVal, 10);
		char e [2];
		e[0] = 'e';
		e[1] = '\0';
		strcat(e, retVal);
		n = write(fd, e, strlen(e));
		if (n < 0){
			error("ERROR writing to socket");
		}
		return -1;
	}
	else {
		char* numstr = (char*)malloc (256);
		itoa (errcheck, numstr, 10);
		char exstorage [strlen(numstr)+numberbytes+1];
		int v = 0;
		while (v < strlen(numstr)){
			exstorage[v] = numstr[v];
			v++;
		}
		exstorage[v] = '|';
		v++;
		int c = 0;
		while (c < errcheck){
			exstorage[v] = storage[c];
			v++;
			c++;
		}
		puts(exstorage);
		n = write (fd, exstorage, errcheck+v);
		if (n < 0){
			error("ERROR writing to socket");
		}
	}
	return (errcheck);
}
int filewrite(char* buffer){
	int n = -1;
	buffer += 2;
	int i = 0;
	char* filedescriptor = (char*) malloc (256);
	while (buffer[i] != '|'){
		filedescriptor[i] = buffer[i];
		i++;
	}
	int fdes = atoi (filedescriptor);
	i++;
	char* numbytes = (char*) malloc (256);
	int j = 0;
	while (buffer[i] != '|'){
		numbytes[j] = buffer[i];
		i++;
		j++;
	}
	int numberbytes = atoi (numbytes);
	i++;
	char* content = (char*) malloc (numberbytes);
	j = 0;
	while (buffer[i] != '\0'){
		content[j] = buffer[i];
		i++;
		j++;
	}
	pthread_mutex_lock(&lock);
	int errcheck = write (fdes, content, numberbytes);
	pthread_mutex_unlock(&lock);
	if (errcheck < 0){
		char* retVal = (char*)malloc(256);
		itoa(errno, retVal, 10);
		char e [2];
		e[0] = 'e';
		e[1] = '\0';
		strcat(e, retVal);
		n = write(fd, e, strlen(e));
		if (n < 0){
			error("ERROR writing to socket");
		}
		return -1;
	}
	else {
		char* numstr = (char*)malloc (256);
		itoa (errcheck, numstr, 10);
		n = write (fd, numstr, 13);
		if (n < 0){
			error("ERROR writing to socket");
		}
	}
	return (errcheck);
}
int fileclose(char* buffer){
	int n = -1;
	char* fdes = (char*)malloc(256);
	int i = 2;
	int j = 0;
	while (buffer[i] != '\0'){
		fdes[j] = buffer[i];
		i++;
		j++;
	}
	int fdesx = atoi (fdes);
	int errcheck = close(fdesx);
	if (errcheck < 0){
		char* retVal = (char*)malloc(256);
		itoa(errno, retVal, 10);
		char e [2];
		e[0] = 'e';
		e[1] = '\0';
		strcat(e, retVal);
		n = write(fd, e, strlen(e));
		if (n < 0){
			error("ERROR writing to socket");
		}
		return -1;
	}
	else {
		n = write (fd, "0", 1);
		if (n < 0){
			error("ERROR writing to socket");
		}
		return 0;
	}
	
}
void* workerThread (void* fdx){
	int* ff = (int*) fdx;
	fd = *ff;
	pthread_detach(pthread_self());
	char buffer[256];
	int n = -1;
	bzero(buffer, 256);
	n = read(fd, buffer, 255);
	if (n < 0){
		error("Error reading from socket.\n");
	}
	if (buffer[0] == 'o' && buffer[1] == '|'){
		fileopen((char*)buffer);
		close(fd);
	}
	else if (buffer[0] == 'r' && buffer[1] == '|'){
		fileread((char*)buffer);
		close(fd);
	}
	else if (buffer[0] == 'w' && buffer[1] == '|'){
		filewrite((char*)buffer);
		close(fd);
	}
	else if (buffer[0] == 'c' && buffer[1] == '|'){
		fileclose((char*)buffer);
		close(fd);
	}
	return NULL;
}
int main(int argc, char *argv[]){
	pthread_mutex_init(&lock, NULL);
	int sockfd = -1;																									
	int portno = -1;													
	int clilen = -1;																																								

	struct sockaddr_in serverAddressInfo;				
	struct sockaddr_in clientAddressInfo;			
	portno = 8888;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0){
       error("ERROR opening socket");
	}
	int optval = 1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int));
	
	bzero((char*)&serverAddressInfo, sizeof(serverAddressInfo));
	serverAddressInfo.sin_port = htons(portno);
    serverAddressInfo.sin_family = AF_INET;
    serverAddressInfo.sin_addr.s_addr = INADDR_ANY;
       
    if (bind(sockfd, (struct sockaddr*) &serverAddressInfo, sizeof(serverAddressInfo)) < 0){
		error("ERROR on binding");
	}		  
    listen(sockfd,5);
	pthread_t tid;
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setscope(&attr,PTHREAD_SCOPE_SYSTEM);	
	int i = 0;
	while(1){
		i = i%1024;
		clilen = sizeof(clientAddressInfo);
		connectionFD[i] = accept(sockfd, (struct sockaddr*)&clientAddressInfo, &clilen);
		pthread_create(&tid, &attr, workerThread, &connectionFD[i]);
		i++;
	}
	return 0; 
}
